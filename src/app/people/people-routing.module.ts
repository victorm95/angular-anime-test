import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CollectionPageComponent } from './containers/collection-page/collection-page.component';
import { CreatePageComponent } from './containers/create-page/create-page.component';

const routes: Routes = [
  {
    path: '',
    component: CollectionPageComponent
  },
  {
    path: 'create',
    component: CreatePageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PeopleRoutingModule {}
