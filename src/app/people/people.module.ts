import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PeopleFormComponent } from './components/people-form/people-form.component';
import { PeopleListComponent } from './components/people-list/people-list.component';
import { CollectionPageComponent } from './containers/collection-page/collection-page.component';
import { CreatePageComponent } from './containers/create-page/create-page.component';
import { PeopleRoutingModule } from './people-routing.module';
import { PeopleService } from './services/people.service';

@NgModule({
  declarations: [
    CollectionPageComponent,
    PeopleListComponent,
    CreatePageComponent,
    PeopleFormComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    PeopleRoutingModule
  ],
  providers: [PeopleService]
})
export class PeopleModule {}
