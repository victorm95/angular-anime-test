import { Component, EventEmitter, Output } from '@angular/core';

import { People } from '../../models/people';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-people-form',
  templateUrl: './people-form.component.html',
  styleUrls: ['./people-form.component.scss']
})
export class PeopleFormComponent {
  @Output()
  save = new EventEmitter<People>();

  form = new FormGroup({
    name: new FormControl('', [Validators.required])
  });

  submit(): void {
    if (this.form.valid) {
      this.save.emit(this.form.value);
    }
  }
}
