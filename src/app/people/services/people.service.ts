import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';
import { People } from '../models/people';

@Injectable()
export class PeopleService {
  apiUrl = `${environment.apiUrl}/people`;

  constructor(private httpClient: HttpClient) {}

  loadPeople(): Observable<People[]> {
    return this.httpClient.get<People[]>(this.apiUrl);
  }

  createPeople(people: People): Observable<People> {
    return this.httpClient.post<People>(this.apiUrl, people);
  }
}
