import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { People } from '../../models/people';
import { PeopleService } from '../../services/people.service';

@Component({
  selector: 'app-create-page',
  templateUrl: './create-page.component.html',
  styleUrls: ['./create-page.component.scss']
})
export class CreatePageComponent {
  constructor(private poepleService: PeopleService, private router: Router) {}

  save(people: People): void {
    this.poepleService.createPeople(people).subscribe(data => {
      this.router.navigate(['/people']);
    });
  }
}
