import { Component } from '@angular/core';

import { PeopleService } from '../../services/people.service';

@Component({
  selector: 'app-collection-page',
  templateUrl: './collection-page.component.html',
  styleUrls: ['./collection-page.component.scss']
})
export class CollectionPageComponent {
  people$ = this.peopleService.loadPeople();

  constructor(private peopleService: PeopleService) {}
}
