import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { FilmListComponent } from './components/film-list/film-list.component';
import { CollectionPageComponent } from './containers/collection-page/collection-page.component';
import { FilmsRoutingModule } from './films-routing.module';
import { FilmsService } from './services/films.service';

@NgModule({
  declarations: [CollectionPageComponent, FilmListComponent],
  imports: [CommonModule, HttpClientModule, FilmsRoutingModule],
  providers: [FilmsService]
})
export class FilmsModule {}
