import { Component } from '@angular/core';

import { FilmsService } from '../../services/films.service';

@Component({
  selector: 'app-collection-page',
  templateUrl: './collection-page.component.html',
  styleUrls: ['./collection-page.component.scss']
})
export class CollectionPageComponent {
  films$ = this.filmsService.loadFilms();

  constructor(private filmsService: FilmsService) {}
}
