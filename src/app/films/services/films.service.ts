import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';
import { Film } from '../models/film';

@Injectable()
export class FilmsService {
  apiUrl = `${environment.apiUrl}/films`;

  constructor(private httpClient: HttpClient) {}

  loadFilms(): Observable<Film[]> {
    return this.httpClient.get<Film[]>(this.apiUrl);
  }
}
