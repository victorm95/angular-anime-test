import { Component, Input } from '@angular/core';

import { Film } from '../../models/film';

@Component({
  selector: 'app-film-list',
  templateUrl: './film-list.component.html',
  styleUrls: ['./film-list.component.scss']
})
export class FilmListComponent {
  @Input()
  films: Film[];
}
